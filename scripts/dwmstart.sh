#!/bin/bash

"$HOME"/.screenlayout/display.sh
xsetroot -cursor_name left_ptr
picom -f &
feh --bg-scale "$HOME"/pictures/wallpapers/arch-black-4k.png --no-fehbg &
exec dwmblocks &
exec dwm
