#
# ~/.bash_profile
#
if [ -z "${DISPLAY}" ] && [ ${XDG_VTNR} -eq 1 ]; then
	exec startx
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc
export PATH=$PATH:$HOME/.local/bin/:$HOME/scripts/:$HOME/tools/Xilinx/Vivado/2020.1/bin/
. "/home/stephen/.local/share/cargo/env"
