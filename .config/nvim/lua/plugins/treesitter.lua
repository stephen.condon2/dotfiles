return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        event = "BufReadPost",
        opts = {
            sync_install = true,
            highlight = { enable = true },
            indent = { enable = true },
            context_commentstring = { enable = true, enable_autocmd = false },
            ensure_installed = {
                "c",
                "cpp",
                "cuda",
                "lua",
                "rust",
                "python",
                "bash",
                "bibtex",
                "markdown",
                "yaml",
                "json",
                "cmake",
                "toml",
                "html",
                "css",
                "r",
                "java",
                "llvm",
                "http",
                "make",
                "vim",
                "sql",
            },
        },
        config = function(_, opts)
            vim.opt.foldmethod = "expr"
            vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
            require("nvim-treesitter.configs").setup(opts)
        end,
    },
}
