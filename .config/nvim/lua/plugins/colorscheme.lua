return {
    { "morhetz/gruvbox",                  lazy = true },
    { "sainnhe/everforest",               lazy = true },
    { "Yazeed1s/oh-lucy.nvim",            lazy = true },
    { "nyoom-engineering/oxocarbon.nvim", lazy = true },
    { "sam4llis/nvim-tundra",             lazy = true },
    { "EdenEast/nightfox.nvim",           lazy = true },
    { "navarasu/onedark.nvim",            lazy = true },
    { "nyoom-engineering/nyoom.nvim",     lazy = true },
    { "nyoom-engineering/oxocarbon.nvim", lazy = true },
    { "rebelot/kanagawa.nvim",            lazy = true },
}
