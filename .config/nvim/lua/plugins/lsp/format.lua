local M = {}

function M.format()
    local buf = vim.api.nvim_get_current_buf()

    vim.lsp.buf.format({
        bufnr = buf,
        filter = function()
            return true
        end,
    })
end

function M.on_attach(client, buf)
    if client.supports_method("textDocument/formatting") then
        vim.api.nvim_create_autocmd("BufWritePre", {
            group = vim.api.nvim_create_augroup("LspFormat." .. buf, {}),
            buffer = buf,
            callback = function()
                if true then
                    M.format()
                end
            end,
        })
    end
end

return M
