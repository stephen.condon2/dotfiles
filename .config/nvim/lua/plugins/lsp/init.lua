-- TODO: Autoformat and Diagnostics
return {
    {
        "neovim/nvim-lspconfig",
        event = "BufReadPre",
        dependencies = {
            { "folke/neodev.nvim", config = true },
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "hrsh7th/cmp-nvim-lsp",
            "p00f/clangd_extensions.nvim",
            { "j-hui/fidget.nvim", config = true },
            "mfussenegger/nvim-jdtls",
            "mason-org/mason-registry",
        },
        opts = {
            servers = {
                clangd = {},
                rust_analyzer = {},
                bashls = {},
                jdtls = {},
                lua_ls = {
                    Lua = {
                        workspace = {
                            checkThirdParty = false,
                            library = {
                                ["/usr/share/nvim/runtime/lua"] = true,
                                ["/usr/share/nvim/runtime/lua/lsp"] = true,
                                ["/usr/share/awesome/lib"] = true,
                            },
                        },
                        completion = {
                            callSnippet = "Replace",
                        },
                    },
                },
                pyright = {},
                cmake = {},
                sqlls = {},
                taplo = {},
                r_language_server = {},
                jsonls = {},
                yamlls = {},
                cssls = {},
                html = {},
                tsserver = {},
            },
            setup = {
                clangd = function(_, opts)
                    require("clangd_extensions").setup({
                        server = opts,
                        extensions = { autoSetHints = false },
                    })
                    return true
                end,
                jdtls = function(_, opts)
                    local capabilities = vim.lsp.protocol.make_client_capabilities()
                    local extendedClientCapabilities = require('jdtls').extendedClientCapabilities
                    extendedClientCapabilities.resolveAdditionalTextEditsSupport = true
                    extendedClientCapabilities.classFileContentsSupport = true
                    vim.api.nvim_create_autocmd("FileType", {
                        pattern = "java",
                        callback = function()
                            local config = {
                                cmd = { "/home/stephen/.local/share/nvim/mason/bin/jdtls" },
                                root_dir = vim.fs.dirname(vim.fs.find({ 'gradlew', '.git', 'mvnw' }, { upward = true })
                                    [1]),
                                settings = {
                                    java = {},
                                    codeGeneration = {
                                        toString = {
                                            template =
                                            "${object.className}{${member.name()}=${member.value}, ${otherMembers}}"
                                        },
                                        hashCodeEquals = {
                                            useInstanceof = true,
                                        },
                                        useBlocks = true,
                                        generateComments = true,
                                    },
                                    extendedClientCapabilities = extendedClientCapabilities,
                                    configuration = {
                                        runtimes = {
                                            {
                                                name = "JavaSE-1.8",
                                                path = "/usr/lib/jvm/java-8-openjdk/",
                                            },
                                            {
                                                name = "JavaSE-17",
                                                path = "/usr/lib/jvm/java-17-openjdk/",
                                            },
                                            {
                                                name = "JavaSE-19",
                                                path = "/usr/lib/jvm/java-19-openjdk/",
                                            },
                                        }
                                    },
                                },
                                handlers = {},
                                capabilities = capabilities,
                            }
                            require("jdtls").start_or_attach(config)
                        end,
                    })
                    return true
                end,
                ["*"] = function(server, opts)
                end,
            },
        },
        config = function(_, opts)
            vim.api.nvim_create_autocmd("LspAttach", {
                callback = function(args)
                    local buffer = args.buf
                    local client = vim.lsp.get_client_by_id(args.data.client_id)
                    require("plugins.lsp.format").on_attach(client, buffer)
                end,
            })

            local servers = opts.servers
            local capabilities =
                require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

            require("mason-lspconfig").setup({
                ensure_installed = vim.tbl_keys(servers),
                automatic_installation = true,
            })

            require("mason-lspconfig").setup_handlers({
                function(server)
                    local server_opts = servers[server] or {}
                    server_opts.capabilities = capabilities
                    if opts.setup[server] then
                        if opts.setup[server](server, server_opts) then
                            return
                        end
                    elseif opts.setup["*"] then
                        if opts.setup["*"](server, server_opts) then
                            return
                        end
                    end
                    require("lspconfig")[server].setup(server_opts)
                end,
            })
        end,
    },

    -- {
    -- 	"jose-elias-alvarez/null-ls.nvim",
    -- 	event = "BufReadPre",
    -- 	dependencies = { "mason.nvim" },
    -- 	opts = function()
    -- 		local nls = require("null-ls")
    -- 		return {
    -- 			sources = {
    -- 				nls.builtins.formatting.stylua,
    -- 				nls.builtins.formatting.rustfmt,
    -- 				nls.builtins.formatting.prettierd,
    -- 				nls.builtins.diagnostics.flake8,
    -- 				nls.builtins.completion.luasnip,
    -- 			},
    -- 		}
    -- 	end,
    -- },

    {
        "williamboman/mason.nvim",
        cmd = "Mason",
        opts = {
            ensure_installed = {
                "stylua",
                "shellcheck",
                "shfmt",
                "flake8",
                "clang-format",
                "rustfmt",
                "prettierd",
                "shellcheck",
                "shfmt",
                "flake8",
            },
        },
        config = function(_, opts)
            require("mason").setup(opts)
            local mr = require("mason-registry")
            for _, tool in ipairs(opts.ensure_installed) do
                local p = mr.get_package(tool)
                if not p:is_installed() then
                    p:install()
                end
            end
        end,
    },
}
