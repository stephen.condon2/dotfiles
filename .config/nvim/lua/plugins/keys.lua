return {
    {
        "mrjones2014/legendary.nvim",
        opts = function()
            local builtin = require("telescope.builtin")
            return {
                keymaps = {
                    {
                        "<leader><space>",
                        { n = builtin.buffers },
                        description = "Telescope: Show buffers",
                    },
                    {
                        "<leader>tf",
                        { n = builtin.find_files },
                        description = "Telescope: Find Files",
                    },
                    {
                        "<leader>tbf",
                        { n = builtin.current_buffer_fuzzy_find },
                        description = "Telescope: Buffer Fuzzy Find",
                    },
                    { "<leader>th",  { n = builtin.help_tags },         description = "Telescope: Help Tags" },
                    {
                        "<leader>tgs",
                        { n = builtin.grep_string },
                        description = "Telescope: Grep String",
                    },
                    { "<leader>tlg", { n = builtin.live_grep },         description = "Telescope: Live Grep" },
                    { "<leader>do",  { n = vim.diagnostic.open_float }, description = "Diagnostic: Open Float" },
                    {
                        "<leader>dp",
                        { n = vim.diagnostic.goto_prev },
                        description = "Diagnostic: Go To Previous Issue",
                    },
                    { "<leader>dn", { n = vim.diagnostic.goto_next }, description = "Diagnostic: Go To Next Issue" },
                    { "<leader>G",  { n = "<cmd>G<cr>" },             description = "Git: Open Git View" },
                    { "<leader>Gb", { n = "<cmd>G blame<cr>" },       description = "Git: Git Blame" },
                    { "<leader>Gd", { n = "<cmd>Gvdiffsplit!<cr>" },  description = "Git: Git Diff View" },
                    {
                        "gD",
                        { n = vim.lsp.buf.declaration },
                        description = "LSP: Go To Declaration",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "gd",
                        { n = vim.lsp.buf.definition },
                        description = "LSP: Go To Definition",
                        opts = { buffer = bufnumber },
                    },
                    { "K",          { n = vim.lsp.buf.hover },     description = "LSP: Hover",
                                                                                                                  opts = {
                            buffer = bufnumber } },
                    {
                        "gi",
                        { n = vim.lsp.buf.implementation },
                        description = "LSP: Go To Implementation",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "gk",
                        { n = vim.lsp.buf.signature_help },
                        description = "LSP: Signature Help",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "<leader>D",
                        { n = vim.lsp.buf.type_definition },
                        description = "LSP: Go To Type Definition",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "<leader>rn",
                        { n = vim.lsp.buf.rename },
                        description = "LSP: Rename Variable",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "gr",
                        { n = vim.lsp.buf.references },
                        description = "LSP: Go To References",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "<leader>ca",
                        { n = vim.lsp.buf.code_action },
                        description = "LSP: Code Action",
                        opts = { buffer = bufnumber },
                    },
                    {
                        "<leader>lf",
                        { n = vim.lsp.buf.format },
                        description = "LSP: Format",
                        opts = { buffer = bufnumber },
                    },
                    { "<C-h>",      { n = "<C-w>h" },              description = "Window: Focus Left" },
                    { "<C-j>",      { n = "<C-w>j" },              description = "Window: Focus Down" },
                    { "<C-k>",      { n = "<C-w>k" },              description = "Window: Focus Up" },
                    { "<C-l>",      { n = "<C-w>l" },              description = "Window: Focus Right" },
                    { "<leader>ft", { n = ":NvimTreeToggle<CR>" }, description = "Nvim Tree: Toggle" },
                    { "-",          { n = require("oil").open },   description = "Oil: Open parent directory" },
                },
            }
        end,
    },
}
