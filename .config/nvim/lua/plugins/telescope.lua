return {
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"stevearc/dressing.nvim",
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
		},
		cmd = "Telescope",
	},
}
