vim.api.nvim_create_augroup("LaTeX", { clear = true })
--vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
--    pattern = "*.tex",
--    command = "VimtexCompile"
--})
--
--vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
--    pattern = "*.tex",
--    command = "compiler textidote|lmake"
--})

vim.g.vimtex_compiler_latexmk = {
    build_dir = '.out',
    options = {
        '-shell-escape',
        '-verbose',
        '-file-line-error',
        '-interaction=nonstopmode',
        '-synctex=1'
    }
}
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_fold_enabled = true

vim.g.vimtex_grammar_textidote = {
    jar = "/usr/share/java/textidote.jar"
}
